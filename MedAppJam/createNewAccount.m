//
//  createNewAccount.m
//  MedAppJam
//
//  Created by App Jam on 11/16/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import "createNewAccount.h"
#import "scripts.h"

@implementation createNewAccount
@synthesize firstName;
@synthesize lastName;
@synthesize username;
@synthesize password;
@synthesize confirmPassword;
@synthesize emailTextField;
@synthesize invalidInput;
@synthesize registrationInvalid;
@synthesize passwordMismatch;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    firstName.placeholder = [NSString stringWithFormat:@"first"];
    firstName.autocorrectionType = UITextAutocorrectionTypeNo;
    
    lastName.placeholder = [NSString stringWithFormat:@"last"];
    lastName.autocorrectionType = UITextAutocorrectionTypeNo;
    
    //username.placeholder = [NSString stringWithFormat:@"username"];
    username.autocorrectionType = UITextAutocorrectionTypeNo;
    
    emailTextField.placeholder = [NSString stringWithFormat:@"johnny.appleseed@apple.com"];
    emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    //password.placeholder = [NSString stringWithFormat:@"password"];
    password.secureTextEntry = YES;
    password.autocorrectionType = UITextAutocorrectionTypeNo;
    
    //confirmPassword.placeholder = [NSString stringWithFormat:@"password"];
    confirmPassword.secureTextEntry = YES;
    confirmPassword.autocorrectionType = UITextAutocorrectionTypeNo;

    firstName.delegate = self;
    lastName.delegate = self;
    username.delegate = self;
    password.delegate = self;
    confirmPassword.delegate = self;
    emailTextField.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

-(BOOL)isEmpty:(UITextField *)input
{
    NSString *trimmedString = [input.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    BOOL empty = [trimmedString isEqualToString:@""];
    return empty;
}

- (IBAction)register:(UIButton *)sender
{
    scripts *s = [[scripts alloc]init];
    int validUsername = [s duplicateUsername:username.text];
    int validRegistration = 0;
    
    if([self isEmpty:username] || [self isEmpty:password] || [self isEmpty:confirmPassword] || [self isEmpty:firstName] || [self isEmpty:lastName] || [self isEmpty:emailTextField])
    {
        validRegistration = 0;
    }
    else
    {
        validRegistration = 1;
    }
    
    if(validRegistration)
    {
        if(validUsername)
        {
            if([confirmPassword.text isEqualToString:password.text])
            {
                [s registerUser:username.text password:password.text firstName:firstName.text lastName:lastName.text email:emailTextField.text];
        
                [self performSegueWithIdentifier:@"registerSegue" sender:self];
                [self dismissViewControllerAnimated:YES completion:NULL];
            }
            else
            {
                invalidInput.text = [NSString stringWithFormat:@""];
                registrationInvalid.text = [NSString stringWithFormat:@""];
                passwordMismatch.text = [NSString stringWithFormat:@"Password mismatch!"];
            }
        }
        else
        {
            invalidInput.text = [NSString stringWithFormat:@"Username already exists"];
            registrationInvalid.text = [NSString stringWithFormat:@""];
            passwordMismatch.text = [NSString stringWithFormat:@""];
        }
    }
    else
    {
        invalidInput.text = [NSString stringWithFormat:@""];
        registrationInvalid.text = [NSString stringWithFormat:@"Invalid Input"];
        passwordMismatch.text = [NSString stringWithFormat:@""];
    }
}

- (IBAction)closeButton:(UIBarButtonItem *)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)dismissKeyboard:(UIButton *)sender
{
    [firstName resignFirstResponder];
    [lastName resignFirstResponder];
    [username resignFirstResponder];
    [password resignFirstResponder];
    [confirmPassword resignFirstResponder];
    [emailTextField resignFirstResponder];
}

- (IBAction)resignAndLogin:(id)sender
{
    UITextField *tf = (UITextField *)sender;
    if (tf.tag == 1)
        [lastName becomeFirstResponder];
	else if (tf.tag == 2)
		[emailTextField becomeFirstResponder];
	else if (tf.tag == 3)
		[username becomeFirstResponder];
	else if (tf.tag == 4)
		[password becomeFirstResponder];
	else if (tf.tag == 5)
		[confirmPassword becomeFirstResponder];
	else if (tf.tag == 6){
		[self register:sender];
	}
	else{
		[firstName becomeFirstResponder];
    }
}

//KEYBOARD SCROLLING
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 200; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
@end
