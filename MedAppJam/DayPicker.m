//
//  DayPicker.m
//  MedAppJam
//
//  Created by Derek Omuro on 11/17/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import "DayPicker.h"
#import <QuartzCore/QuartzCore.h>

@implementation DayPicker
@synthesize frequencyPicker;
@synthesize pickerArray;

DataManager *dataStorage;
- (void)viewDidLoad
{
    [self.view.superview setBounds:CGRectMake(0, 0, 540, 300)];
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    
    pickerArray = [NSArray arrayWithObjects:@"Once a day", @"Once every two days", @"Once every three days", @"Once every four days", @"Once every five days",@"Once every six days", @"Once a week", @"Once every two weeks", @"Once every three weeks", @"Once every four weeks", nil];
    
    [frequencyPicker reloadAllComponents];
    
    dataStorage = [DataManager sharedInstance];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    //One column
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    //set number of rows
    return pickerArray.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //set item per row
    return [pickerArray objectAtIndex:row];
}

- (IBAction)done:(UIBarButtonItem *)sender {
    //Set dates and close window
    NSInteger row;
    NSString *frequency;
    
    NSArray *frequencyArray = [[NSArray alloc] initWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"14", @"21", @"28", nil];
    
    row = [frequencyPicker selectedRowInComponent:0];
    frequency = [pickerArray objectAtIndex:row];
    NSString *days = [frequencyArray objectAtIndex:row];
    
    dataStorage.freqDay = days;
    dataStorage.frequency = frequency;
    [self close:nil];
}

- (IBAction)cancel:(UIBarButtonItem *)sender {
    //Do not set dates and close window
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(IBAction)close:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateLabels"
                                                        object:nil
                                                      userInfo:nil];
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
