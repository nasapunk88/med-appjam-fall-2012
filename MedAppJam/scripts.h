//
//  scripts.h
//  MedAppJam
//
//  Created by Chris Miliotti on 11/17/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface scripts : NSObject

-(NSString *) postData:(NSString *)script parameter:(NSString *)parameter;
-(int) registerUser:(NSString *)username password:(NSString *) password firstName:(NSString *) firstName lastName:(NSString *) lastName email:(NSString *) email;
-(int) login:(NSString *)username password:(NSString *) password;
-(int) duplicateUsername:(NSString *)username;
-(int) usernameValid:(NSString *)username;
-(int) forgotPassword:(NSString *)username;
-(NSString *) getPrescriptions:(NSString *)number;
-(int) createPrescription:(NSString *)number name:(NSString *)name frequency:(NSString *)frequency time:(NSString *)time start:(NSString *)start end:(NSString *)end notes:(NSString *)notes;
@end