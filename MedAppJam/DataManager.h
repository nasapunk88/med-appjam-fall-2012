//
//  DataManager.h
//  MedAppJam
//
//  Created by Derek Omuro on 11/18/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataManager : UIViewController

@property (nonatomic, retain) NSString *startDate;
@property (nonatomic, retain) NSString *endDate;
@property (nonatomic, retain) NSString *frequency;
@property (nonatomic, retain) NSString *timesPerDay;
@property (nonatomic, retain) NSString *freqDay;

@property (nonatomic, retain) NSString *timeFirstLabel;
@property (nonatomic, retain) NSString *timeSecondLabel;
@property (nonatomic, retain) NSString *timeThirdLabel;
@property (nonatomic, retain) NSString *timeFourthLabel;
@property (nonatomic, retain) NSString *timeFifthLabel;
@property (nonatomic, retain) NSString *timeSixthLabel;
@property (nonatomic, retain) NSString *timeSeventhLabel;

+(id) sharedInstance;

@end
