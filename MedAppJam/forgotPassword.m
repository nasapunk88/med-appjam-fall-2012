//
//  forgotPassword.m
//  MedAppJam
//
//  Created by App Jam on 11/17/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import "forgotPassword.h"
#import <QuartzCore/QuartzCore.h>
#import "scripts.h"

@interface forgotPassword ()

@end

@implementation forgotPassword
@synthesize usernameTextField;
@synthesize confirmationLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    usernameTextField.placeholder = [NSString stringWithFormat:@"Enter your username"];
    usernameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

- (IBAction)submitButton:(UIButton *)sender
{
    scripts *s = [[scripts alloc]init];
    int validUsername = [s usernameValid:usernameTextField.text];
    
    if(validUsername)
    {
        confirmationLabel.text = [NSString stringWithFormat:@"Your password has been emailed to you."];
    }
    else
    {
        confirmationLabel.text = [NSString stringWithFormat:@"Username not found"];
    }
}
- (IBAction)cancel:(UIBarButtonItem *)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)dismissKeyboard:(UIButton *)sender
{
    [usernameTextField resignFirstResponder];
}
@end
