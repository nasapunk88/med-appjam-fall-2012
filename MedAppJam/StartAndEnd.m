//
//  StartAndEnd.m
//  MedAppJam
//
//  Created by Derek Omuro on 11/17/12.
//  Copyright (c) 2012 Team 15. All rights reserved.

#import "StartAndEnd.h"
#import <QuartzCore/QuartzCore.h>

@implementation StartAndEnd
@synthesize datePicker;
@synthesize tableBackground;
@synthesize tableView;
@synthesize startDateLabel, endDateLabel;

int gFieldSelected = 0;
UIButton *lastButton;
DataManager *dataStorage;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    tableBackground.layer.cornerRadius = 15;
    tableBackground.layer.masksToBounds = YES;

    tableView.scrollEnabled = NO;
    
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    dataStorage = [DataManager sharedInstance];
    if(dataStorage.startDate != nil)
    {
        startDateLabel.text = dataStorage.startDate;
        startDateLabel.textColor = [UIColor blueColor];
    }
    if(dataStorage.endDate != nil)
    {
        endDateLabel.text = dataStorage.endDate;
        endDateLabel.textColor = [UIColor blueColor];
    }
    
    tableView.allowsSelection = true;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startDateButton:(UIButton *)sender {
    
    sender.backgroundColor = [UIColor lightGrayColor];
    lastButton.backgroundColor = [UIColor whiteColor];
    lastButton = sender;

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *stringFromDate = [dateFormatter stringFromDate:datePicker.date];
    startDateLabel.text = stringFromDate;
    startDateLabel.textColor = [UIColor blueColor];
    gFieldSelected = 0;
}
- (IBAction)endDateButton:(UIButton *)sender {
    
    sender.backgroundColor = [UIColor lightGrayColor];
    lastButton.backgroundColor = [UIColor whiteColor];
    lastButton = sender;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *stringFromDate = [dateFormatter stringFromDate:datePicker.date];
    endDateLabel.text = stringFromDate;
    endDateLabel.textColor = [UIColor blueColor];
    gFieldSelected = 1;
}


- (IBAction)done:(UIBarButtonItem *)sender {
    //Set dates and close window
    dataStorage.startDate = startDateLabel.text;
    dataStorage.endDate = endDateLabel.text;
    [self close:nil];
}
- (IBAction)cancel:(UIBarButtonItem *)sender {
    //Do not set dates and close window
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (IBAction)datePickerUpdate:(UIDatePicker *)sender {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *stringFromDate = [dateFormatter stringFromDate:datePicker.date];
    switch (gFieldSelected)
    {
        case 0:
            startDateLabel.text = stringFromDate;
            startDateLabel.textColor = [UIColor blueColor];
            break;
        case 1:
            endDateLabel.text = stringFromDate;
            endDateLabel.textColor = [UIColor blueColor];
        default:
            break;
    }
}

-(IBAction)close:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateLabels"
                                                        object:nil
                                                      userInfo:nil];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end