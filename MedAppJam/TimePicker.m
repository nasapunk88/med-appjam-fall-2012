//
//  TimePicker.m
//  MedAppJam
//
//  Created by Derek Omuro on 11/17/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import "TimePicker.h"
#import <QuartzCore/QuartzCore.h>

@implementation TimePicker
@synthesize timePicker;
@synthesize timeTable;
@synthesize tableBackground;
@synthesize firstLabel;
@synthesize secondLabel;
@synthesize thirdLabel;
@synthesize fourthLabel;
@synthesize fifthLabel;
@synthesize sixthLabel;
@synthesize seventhLabel;

int cell = 1;
UIButton *lastButton;
DataManager *dataStorage;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    tableBackground.layer.cornerRadius = 15;
    tableBackground.layer.masksToBounds = YES;
    [timePicker setDatePickerMode:UIDatePickerModeTime];
    dataStorage = [DataManager sharedInstance];
    
    if(dataStorage.timeFirstLabel != nil){
        firstLabel.text = dataStorage.timeFirstLabel;
    }
    if(dataStorage.timeSecondLabel != nil){
        secondLabel.text = dataStorage.timeSecondLabel;
    }
    if(dataStorage.timeThirdLabel != nil){
        thirdLabel.text = dataStorage.timeThirdLabel;
    }
    if(dataStorage.timeFourthLabel != nil){
        fourthLabel.text = dataStorage.timeFourthLabel;
    }
    if(dataStorage.timeFifthLabel != nil){
        fifthLabel.text = dataStorage.timeFifthLabel;
    }
    if(dataStorage.timeSixthLabel != nil){
        sixthLabel.text = dataStorage.timeSixthLabel;
    }
    if(dataStorage.timeSeventhLabel != nil){
        seventhLabel.text = dataStorage.timeSeventhLabel;
    }

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel:(UIBarButtonItem *)sender
{
    dataStorage.timeFirstLabel = nil;
    dataStorage.timeSecondLabel = nil;
    dataStorage.timeThirdLabel = nil;
    dataStorage.timeFourthLabel= nil;
    dataStorage.timeFifthLabel = nil;
    dataStorage.timeSixthLabel = nil;
    dataStorage.timeSeventhLabel = nil;
    dataStorage.timesPerDay = [NSString stringWithFormat:@"0 per day"];

    [self close:nil];
}

- (IBAction)done:(UIBarButtonItem *)sender
{
    int count = 0;
    if(![firstLabel.text isEqualToString:@""]){
        dataStorage.timeFirstLabel = firstLabel.text;
        count++;
    }
    if(![secondLabel.text isEqualToString:@""]){
        dataStorage.timeSecondLabel = secondLabel.text;
        count++;
    }
    if(![thirdLabel.text isEqualToString:@""]){
        dataStorage.timeThirdLabel = thirdLabel.text;
        count++;
    }
    if(![fourthLabel.text isEqualToString:@""]){
        dataStorage.timeFourthLabel = fourthLabel.text;
        count++;
    }
    if(![fifthLabel.text isEqualToString:@""]){
        dataStorage.timeFifthLabel = fifthLabel.text;
        count++;
    }
    if(![sixthLabel.text isEqualToString:@""]){
        dataStorage.timeSixthLabel = sixthLabel.text;
        count++;
    }
    if(![seventhLabel.text isEqualToString:@""]){
        dataStorage.timeSeventhLabel = seventhLabel.text;
        count++;
    }
    
    dataStorage.timesPerDay = [NSString stringWithFormat:@"%d per day", count];
    [self close:nil];
}

- (IBAction)firstButton:(UIButton *)sender
{    
    cell = 1;
    sender.backgroundColor = [UIColor lightGrayColor];
    lastButton.backgroundColor = [UIColor whiteColor];
    lastButton = sender;
}

- (IBAction)secondButton:(UIButton *)sender
{
    cell = 2;
    sender.backgroundColor = [UIColor lightGrayColor];
    lastButton.backgroundColor = [UIColor whiteColor];
    lastButton = sender;
}

- (IBAction)thirdButton:(UIButton *)sender
{
    cell = 3;
    sender.backgroundColor = [UIColor lightGrayColor];
    lastButton.backgroundColor = [UIColor whiteColor];
    lastButton = sender;
}

- (IBAction)fourthButton:(UIButton *)sender
{
    cell = 4;
    sender.backgroundColor = [UIColor lightGrayColor];
    lastButton.backgroundColor = [UIColor whiteColor];
    lastButton = sender;
}

- (IBAction)fifthButton:(UIButton *)sender
{
    cell = 5;
    sender.backgroundColor = [UIColor lightGrayColor];
    lastButton.backgroundColor = [UIColor whiteColor];
    lastButton = sender;
}

- (IBAction)sixthButton:(UIButton *)sender
{
    cell = 6;
    sender.backgroundColor = [UIColor lightGrayColor];
    lastButton.backgroundColor = [UIColor whiteColor];
    lastButton = sender;
}

- (IBAction)seventhButton:(UIButton *)sender
{
    cell = 7;
    sender.backgroundColor = [UIColor lightGrayColor];
    lastButton.backgroundColor = [UIColor whiteColor];
    lastButton = sender;
}

- (IBAction)timePicker:(UIDatePicker *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"hh:mm a" options:0 locale:[NSLocale currentLocale]]];
    NSString *stringFromDate = [dateFormatter stringFromDate:timePicker.date];
    
    if(cell == 1)
        firstLabel.text = stringFromDate;
    else if(cell == 2)
        secondLabel.text = stringFromDate;
    else if(cell == 3)
        thirdLabel.text = stringFromDate;
    else if(cell == 4)
        fourthLabel.text = stringFromDate;
    else if(cell == 5)
        fifthLabel.text = stringFromDate;
    else if(cell == 6)
        sixthLabel.text = stringFromDate;
    else if(cell == 7)
        seventhLabel.text = stringFromDate;
}

-(IBAction)close:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateLabels"
                                                        object:nil
                                                      userInfo:nil];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
