//
//  slotMachine.h/Users/derekomuro/Dropbox/MedAppJam/MedAppJam/MedAppJam/TimePicker.h
//  MedAppJam
//
//  Created by App Jam on 11/16/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

@interface slotMachine : UIViewController <UITextViewDelegate, UITableViewDelegate>{
}

@property (nonatomic, retain) NSArray *cells;
@property (strong, nonatomic) IBOutlet UITextView *notes;
@property (strong, nonatomic) IBOutlet UIView *notesBackground;
@property (strong, nonatomic) IBOutlet UITableView *prescriptionHistoryTable;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *RXTextField;
@property (strong, nonatomic) IBOutlet UILabel *startDateSlot;
@property (strong, nonatomic) IBOutlet UILabel *endDateSlot;
@property (strong, nonatomic) IBOutlet UILabel *frequencySlot;
@property (strong, nonatomic) IBOutlet UILabel *timesPerDaySlot;
@property (strong, nonatomic) NSMutableArray *exactTimesPerDaySlot;
@property (strong, nonatomic) IBOutlet UITextView *commentTextField;
@property (nonatomic, retain) DataManager *dataStorage;
@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UILabel *label3;
@property (strong, nonatomic) IBOutlet UILabel *label4;
@property (strong, nonatomic) IBOutlet UILabel *label5;
@property (strong, nonatomic) IBOutlet UILabel *label6;
@property (strong, nonatomic) IBOutlet UILabel *label7;
@property (strong, nonatomic) IBOutlet UILabel *label8;
@property (strong, nonatomic) IBOutlet UILabel *label9;
@property (strong, nonatomic) IBOutlet UILabel *label10;
@property (strong, nonatomic) IBOutlet UILabel *label11;
@property (strong, nonatomic) IBOutlet UILabel *label12;
@property (strong, nonatomic) IBOutlet UILabel *label13;
@property (strong, nonatomic) IBOutlet UILabel *label14;
@property (strong, nonatomic) IBOutlet UILabel *label15;


- (IBAction)dismissKeyboard:(UIButton *)sender;
- (IBAction)createReminderButton:(UIButton *)sender;
- (IBAction)logoutButton:(UIBarButtonItem *)sender;
- (IBAction)startAndEnd:(UIButton *)sender;
- (IBAction)dayPicker:(UIButton *)sender;
- (IBAction)timePicker:(UIButton *)sender;
- (IBAction)button1:(UIButton *)sender;
- (IBAction)button2:(UIButton *)sender;
- (IBAction)button3:(UIButton *)sender;
- (IBAction)button4:(UIButton *)sender;
- (IBAction)button5:(UIButton *)sender;
- (IBAction)button6:(UIButton *)sender;
- (IBAction)button7:(UIButton *)sender;
- (IBAction)button8:(UIButton *)sender;
- (IBAction)button9:(UIButton *)sender;
- (IBAction)button10:(UIButton *)sender;
- (IBAction)button11:(UIButton *)sender;
- (IBAction)button12:(UIButton *)sender;
- (IBAction)button13:(UIButton *)sender;
- (IBAction)button14:(UIButton *)sender;
- (IBAction)button15:(UIButton *)sender;

- (void)updateLabels;
- (void)updateTextFields;
- (IBAction)phoneNumberLookup:(UITextField *)sender;
@end
