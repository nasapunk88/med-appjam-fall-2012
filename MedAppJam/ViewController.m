//
//  ViewController.m
//  MedAppJam
//
//  Created by Derek Omuro on 11/16/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ViewController.h"
#import "slotMachine.h"
#import "createNewAccount.h"
#import "scripts.h"

@implementation ViewController
@synthesize usernameTextField;
@synthesize passwordTextField;
@synthesize tableBackground;
@synthesize loginTable;
@synthesize incorrectLoginLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    usernameTextField.placeholder = [NSString stringWithFormat:@"username"];
    usernameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    passwordTextField.placeholder = [NSString stringWithFormat:@"password"];
    passwordTextField.secureTextEntry = YES;
    passwordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    tableBackground.layer.cornerRadius = 15;
    tableBackground.layer.masksToBounds = YES;
    
    loginTable.userInteractionEnabled = NO;
    
    usernameTextField.delegate = self;
    passwordTextField.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButton:(UIButton *)sender
{
	scripts *s = [[scripts alloc]init];
	int validLogin = [s login:usernameTextField.text password:passwordTextField.text];
    
    if (validLogin)
    {
        [self performSegueWithIdentifier:@"loginSegue" sender:sender];
    }
    else
    {
        incorrectLoginLabel.text = [NSString stringWithFormat:@"Incorrect Username or Password"];
        [passwordTextField setText:@""];
    }
}

- (IBAction)createAccountButton:(UIButton *)sender
{
    
}

- (IBAction)forgotPasswordButton:(UIButton *)sender
{
    
}

- (IBAction)dismissKeyboard:(UIButton *)sender
{
    [usernameTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
}
//  When we are done editing on the keyboard
- (IBAction)resignAndLogin:(id)sender
{
    //  Get a reference to the text field on which the done button was pressed
    UITextField *tf = (UITextField *)sender;
	
    //  Check the tag. If this is the username field, then jump to the password field automatically
    if (tf.tag == 1)
	{
		
        [passwordTextField becomeFirstResponder];
		
		//  Otherwise we pressed done on the password field, and want to attempt login
    }
	else {
		
        //  First put away the keyboard
        [sender resignFirstResponder];
		
        scripts *s = [[scripts alloc]init];
		int webresult = [s login:usernameTextField.text password:passwordTextField.text];
		
        //  Actually run the query in Core Data and return the count of found users with these details
        //  Obviously if it found ANY then we got the username and password right!
        if ( webresult == 1)
		{
            //  We found a matching login user!  Force the segue transition to the next view
			NSLog(@"user found");
            [self performSegueWithIdentifier:@"loginSegue" sender:sender];
		}
        else
		{
			NSLog(@"user NOT found");
            //  We didn't find any matching login users. Wipe the password field to re-enter
            [passwordTextField setText:@""];
		}
    }
}

//  When the view reappears after logout we want to wipe the username and password fields
- (void)viewWillAppear:(BOOL)animated
{
    [usernameTextField setText:@""];
    [passwordTextField setText:@""];
}


//KEYBOARD SCROLLING
- (void)textFieldDidBeginEditing:(UITextField *)usernameTextField
{
    [self animateTextField: self.usernameTextField up: YES];
}


- (void)textFieldDidEndEditing:(UITextField *)usernameTextField
{
    [self animateTextField: self.usernameTextField up: NO];
}

- (void) animateTextField: (UITextField*) usernameTextField up: (BOOL) up
{
    const int movementDistance = 150; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, movement, 0);
    [UIView commitAnimations];
}

@end