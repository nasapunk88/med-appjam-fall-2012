//
//  main.m
//  MedAppJam
//
//  Created by Derek Omuro on 11/16/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
