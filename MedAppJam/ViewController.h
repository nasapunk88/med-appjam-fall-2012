//
//  ViewController.h
//  MedAppJam
//
//  Created by Derek Omuro on 11/16/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableView *loginTable;
@property (strong, nonatomic) IBOutlet UIView *tableBackground;
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UILabel *incorrectLoginLabel;

- (IBAction)loginButton:(UIButton *)sender;
- (IBAction)createAccountButton:(UIButton *)sender;
- (IBAction)forgotPasswordButton:(UIButton *)sender;
- (IBAction)dismissKeyboard:(UIButton *)sender;

@end
