//
//  slotMachine.m
//  MedAppJam
//
//  Created by App Jam on 11/16/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import "slotMachine.h"
#import <QuartzCore/QuartzCore.h>
#import "scripts.h"

@implementation slotMachine
@synthesize notes;
@synthesize notesBackground;
@synthesize phoneNumberTextField;
@synthesize RXTextField;
@synthesize prescriptionHistoryTable;
@synthesize startDateSlot, endDateSlot, frequencySlot, timesPerDaySlot, exactTimesPerDaySlot;
@synthesize commentTextField;
@synthesize dataStorage;
@synthesize cells;
@synthesize label1, label2, label3, label4, label5, label6, label7, label8, label9, label10, label11, label12, label13, label14, label15;

NSArray *array;
NSArray *labels;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    dataStorage = [DataManager sharedInstance];
    array = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:15], nil];
    
    labels = [[NSArray alloc] initWithObjects:label1, label2, label3, label4, label5, label6, label7, label8, label9, label10, label11, label12, label13, label14, label15, nil];
    
    phoneNumberTextField.placeholder = [NSString stringWithFormat:@"Phone number"];
    phoneNumberTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    phoneNumberTextField.keyboardType = UIKeyboardTypePhonePad;
    
    RXTextField.placeholder = [NSString stringWithFormat:@"Drug name"];
    RXTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    [[self.notes layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.notes layer] setBorderWidth:1.5];
    [[self.notes layer] setCornerRadius:15];
    
    notes.delegate = self;
    phoneNumberTextField.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                          selector:@selector(updateLabels)
                                          name:@"updateLabels"
                                          object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)updateLabels
{
    if(dataStorage.startDate != nil)
    {
        startDateSlot.text = dataStorage.startDate;
        startDateSlot.textColor = [UIColor blueColor];
    }
    if(dataStorage.endDate != nil)
    {
        endDateSlot.text = dataStorage.endDate;
        endDateSlot.textColor = [UIColor blueColor];
    }
    if(dataStorage.frequency != nil)
    {
        frequencySlot.text = dataStorage.frequency;
        frequencySlot.textColor = [UIColor blueColor];
    }
    if(dataStorage.timesPerDay != nil)
    {
        timesPerDaySlot.text = dataStorage.timesPerDay;
        timesPerDaySlot.textColor = [UIColor blueColor];
    }
}

- (IBAction)dismissKeyboard:(UIButton *)sender
{
    [notes resignFirstResponder];
    [phoneNumberTextField resignFirstResponder];
    [RXTextField resignFirstResponder];
}

- (IBAction)createReminderButton:(UIButton *)sender
{
    NSMutableArray *timeArray = [[NSMutableArray alloc] init];
    
    scripts *newScript = [[scripts alloc] init];
    if (dataStorage.timeFirstLabel != nil)
    {
        [timeArray addObject:dataStorage.timeFirstLabel];
    }
    if (dataStorage.timeSecondLabel != nil)
    {
        [timeArray addObject:dataStorage.timeSecondLabel];
    }
    if (dataStorage.timeThirdLabel != nil)
    {
        [timeArray addObject:dataStorage.timeThirdLabel];
    }
    if (dataStorage.timeFourthLabel != nil)
    {
        [timeArray addObject:dataStorage.timeFourthLabel];
    }
    if (dataStorage.timeFifthLabel != nil)
    {
        [timeArray addObject:dataStorage.timeFifthLabel];
    }
    if (dataStorage.timeSixthLabel != nil)
    {
        [timeArray addObject:dataStorage.timeSixthLabel];
    }
    if (dataStorage.timeSeventhLabel != nil)
    {
        [timeArray addObject:dataStorage.timeSeventhLabel];
    }
    
    for(int i = 0; i < [timeArray count]; i++)
    {
        [newScript createPrescription:phoneNumberTextField.text name:RXTextField.text frequency:dataStorage.freqDay time:[timeArray objectAtIndex:i] start:dataStorage.startDate end:dataStorage.endDate notes:notes.text];
    }
    
    [self updateTextFields];
}

- (IBAction)logoutButton:(UIBarButtonItem *)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)startAndEnd:(UIButton *)sender
{
    dataStorage.startDate = startDateSlot.text;
    dataStorage.endDate = endDateSlot.text;
}
- (IBAction)dayPicker:(UIButton *)sender
{
    
}

- (IBAction)timePicker:(UIButton *)sender {
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSNumber* candidateNumber;
    
    NSString* candidateString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    range = NSMakeRange(0, [candidateString length]);
    
    [numberFormatter getObjectValue:&candidateNumber forString:candidateString range:&range error:nil];
    
    if (([candidateString length] > 0) && (candidateNumber == nil || range.length < [candidateString length])) {
        
        return NO;
    }
    else
    {
        return YES;
    }
}

//KEYBOARD SCROLLER
- (void)textViewDidBeginEditing:(UITextView *)notes
{
    [self animateTextView: self.notes up: YES];
}

- (void)textViewDidEndEditing:(UITextView *)notes
{
    [self animateTextView: self.notes up: NO];
}

- (void) animateTextView: (UITextView*) notes up: (BOOL) up
{
    const int movementDistance = 235; // tweak as needed
    const float movementDuration = 0.2f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, movement, 0);
    [UIView commitAnimations];
}

- (void)updateTextFields
{
    scripts *newScript = [[scripts alloc] init];
    NSString *list = [newScript getPrescriptions:phoneNumberTextField.text];
    array =[list componentsSeparatedByString:@"@"];
    UILabel *currentLabel = [[UILabel alloc] init];
    
    for(int i = 0; i < [array count]-1 && i < 15; i++)
    {
        NSArray *row = [[array objectAtIndex:i] componentsSeparatedByString:@","];
        currentLabel = [labels objectAtIndex:i];
        currentLabel.text = [NSString stringWithFormat:@"%@   %@", row[0], row[3]];
    }
}

- (IBAction)button1:(UIButton *)sender
{
    
}

- (IBAction)button2:(UIButton *)sender {
}

- (IBAction)button3:(UIButton *)sender {
}

- (IBAction)button4:(UIButton *)sender {
}

- (IBAction)button5:(UIButton *)sender {
}

- (IBAction)button6:(UIButton *)sender {
}

- (IBAction)button7:(UIButton *)sender {
}

- (IBAction)button8:(UIButton *)sender {
}

- (IBAction)button9:(UIButton *)sender {
}

- (IBAction)button10:(UIButton *)sender {
}

- (IBAction)button11:(UIButton *)sender {
}

- (IBAction)button12:(UIButton *)sender {
}

- (IBAction)button13:(UIButton *)sender {
}

- (IBAction)button14:(UIButton *)sender {
}

- (IBAction)button15:(UIButton *)sender {
}

- (IBAction)phoneNumberLookup:(UITextField *)sender
{
    [self updateTextFields];
}
@end
