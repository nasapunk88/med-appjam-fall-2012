//
//  StartAndEnd.h
//  MedAppJam
//
//  Created by Derek Omuro on 11/17/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

@interface StartAndEnd : UIViewController
{
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *tableBackground;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UILabel *startDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *endDateLabel;

- (IBAction)startDateButton:(UIButton *)sender;
- (IBAction)done:(UIBarButtonItem *)sender;
- (IBAction)cancel:(UIBarButtonItem *)sender;
- (IBAction)datePickerUpdate:(UIDatePicker *)sender;
- (IBAction)endDateButton:(UIButton *)sender;
- (IBAction)close:(id)sender;

@end
