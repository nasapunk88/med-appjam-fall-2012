//
//  DayPicker.h
//  MedAppJam
//
//  Created by Derek Omuro on 11/17/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

@interface DayPicker: UIViewController<UIPickerViewDataSource, UIPickerViewDelegate> {
}
@property (strong, nonatomic) IBOutlet UIPickerView *frequencyPicker;
@property NSArray *pickerArray;
- (IBAction)done:(UIBarButtonItem *)sender;
- (IBAction)cancel:(UIBarButtonItem *)sender;
- (IBAction)close:(id)sender;
@end