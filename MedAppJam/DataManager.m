//
//  DataManager.m
//  MedAppJam
//
//  Created by Derek Omuro on 11/18/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager
@synthesize startDate, endDate, frequency, timesPerDay;
@synthesize timeFirstLabel,timeSecondLabel, timeThirdLabel, timeFourthLabel, timeFifthLabel, timeSixthLabel, timeSeventhLabel;

+(id) sharedInstance
{
    static id sharedInstance = nil;
    if (sharedInstance == nil) {
        sharedInstance = [[self alloc] init];
    }
    return sharedInstance;
}

//and when you allocate a new instance in another view controller do this

@end
