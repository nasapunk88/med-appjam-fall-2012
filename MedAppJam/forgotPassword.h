//
//  forgotPassword.h
//  MedAppJam
//
//  Created by App Jam on 11/17/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface forgotPassword : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UILabel *confirmationLabel;

- (IBAction)submitButton:(UIButton *)sender;
- (IBAction)cancel:(UIBarButtonItem *)sender;
- (IBAction)dismissKeyboard:(UIButton *)sender;

@end
