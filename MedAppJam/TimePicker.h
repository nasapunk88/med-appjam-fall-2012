//
//  TimePicker.h
//  MedAppJam
//
//  Created by Derek Omuro on 11/17/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

@interface TimePicker : UIViewController
@property (strong, nonatomic) IBOutlet UIDatePicker *timePicker;
@property (strong, nonatomic) IBOutlet UITableView *timeTable;
@property (strong, nonatomic) IBOutlet UIView *tableBackground;

@property (strong, nonatomic) IBOutlet UILabel *firstLabel;
@property (strong, nonatomic) IBOutlet UILabel *secondLabel;
@property (strong, nonatomic) IBOutlet UILabel *thirdLabel;
@property (strong, nonatomic) IBOutlet UILabel *fourthLabel;
@property (strong, nonatomic) IBOutlet UILabel *fifthLabel;
@property (strong, nonatomic) IBOutlet UILabel *sixthLabel;
@property (strong, nonatomic) IBOutlet UILabel *seventhLabel;

- (IBAction)cancel:(UIBarButtonItem *)sender;
- (IBAction)done:(UIBarButtonItem *)sender;
- (IBAction)timePicker:(UIDatePicker *)sender;

- (IBAction)firstButton:(UIButton *)sender;
- (IBAction)secondButton:(UIButton *)sender;
- (IBAction)thirdButton:(UIButton *)sender;
- (IBAction)fourthButton:(UIButton *)sender;
- (IBAction)fifthButton:(UIButton *)sender;
- (IBAction)sixthButton:(UIButton *)sender;
- (IBAction)seventhButton:(UIButton *)sender;

- (IBAction)close:(id)sender;

@end
