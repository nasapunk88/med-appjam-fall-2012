//
//  createNewAccount.h
//  MedAppJam
//
//  Created by App Jam on 11/16/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface createNewAccount : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *lastName;
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *confirmPassword;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UILabel *invalidInput;
@property (strong, nonatomic) IBOutlet UILabel *registrationInvalid;
@property (strong, nonatomic) IBOutlet UILabel *passwordMismatch;

- (IBAction)register:(UIButton *)sender;
- (IBAction)closeButton:(UIBarButtonItem *)sender;
- (IBAction)dismissKeyboard:(UIButton *)sender;

@end
