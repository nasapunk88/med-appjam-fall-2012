//
//  scripts.m
//  MedAppJam
//
//  Created by Chris Miliotti on 11/17/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import "scripts.h"

@implementation scripts

-(int) registerUser:(NSString *)username password:(NSString *) password firstName:(NSString *) firstName lastName:(NSString *) lastName email:(NSString *) email
{
	NSString *script = [NSString stringWithFormat:@"register.php"];
	NSString *parameter = [NSString stringWithFormat:@"username=%@&password=%@&email=%@",username,password,email];
	return [[self postData:script parameter:parameter] intValue];
}

-(int) forgotPassword:(NSString *)username
{
    NSString *script = [NSString stringWithFormat:@"forgotPassword.php"];
	NSString *parameter = [NSString stringWithFormat:@"username=%@",username];
	return [[self postData:script parameter:parameter] intValue];
}

-(int) usernameValid:(NSString *)username
{
    NSString *script = [NSString stringWithFormat:@"forgotPassword.php"];
	NSString *parameter = [NSString stringWithFormat:@"username=%@",username];
	return [[self postData:script parameter:parameter] intValue];
}

-(int) duplicateUsername:(NSString *)username
{
    NSString *script = [NSString stringWithFormat:@"forgotPassword.php"];
	NSString *parameter = [NSString stringWithFormat:@"username=%@",username];
    int valid = [[self postData:script parameter:parameter] intValue];
    
    if(valid)
        return 0;
    else
        return 1;
}

-(int) login:(NSString *)username password:(NSString *)password
{
	NSString *script = [NSString stringWithFormat:@"login.php"];
	NSString *parameter = [NSString stringWithFormat:@"username=%@&password=%@",username,password];
	return [[self postData:script parameter:parameter] intValue];
}

-(NSString *) getPrescriptions:(NSString *)number
{
    NSString *script = [NSString stringWithFormat:@"getPrescriptions.php"];
	NSString *parameter = [NSString stringWithFormat:@"number=%@",number];
	return [self postData:script parameter:parameter];
}
-(int) createPrescription:(NSString *)number name:(NSString *)name frequency:(NSString *)frequency time:(NSString *)time start:(NSString *)start end:(NSString *)end notes:(NSString *)notes
{
    
	NSString *script = [NSString stringWithFormat:@"createPrescription.php"];
	NSString *parameter = [NSString stringWithFormat:@"number=%@&name=%@&frequency=%@&time=%@&start=%@&end=%@&notes=%@",number,name,frequency,time,start,end, notes];
	return [[self postData:script parameter:parameter] intValue];
}

-(NSString*) postData:(NSString *)script parameter:(NSString *)parameter
{
	NSString *post=parameter;
	//NSLog(@"post string is:%@",post);
	NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
	
	NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
	
	NSString *url = [NSString stringWithFormat:@"%s%@", "http://appjam.site50.net/",script];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
	[request setURL:[NSURL URLWithString:url]];
	[request setHTTPMethod:@"POST"];
	[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	[request setHTTPBody:postData];
	NSURLResponse *response;
	NSError *error;
	NSData *serverReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	NSString *replyString = [[NSString alloc] initWithBytes:[serverReply bytes] length:[serverReply length] encoding: NSASCIIStringEncoding];
	NSLog(@"reply string is :%@",replyString);
	return replyString;
	
}
@end
