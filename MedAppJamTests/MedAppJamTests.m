//
//  MedAppJamTests.m
//  MedAppJamTests
//
//  Created by Derek Omuro on 11/16/12.
//  Copyright (c) 2012 Team 15. All rights reserved.
//

#import "MedAppJamTests.h"

@implementation MedAppJamTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in MedAppJamTests");
}

@end
